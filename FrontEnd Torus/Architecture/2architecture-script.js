/* WEB MENU */

$('.menu-icon').on('click', function () {
    $(".overlayWeb").fadeIn("slow");
    $("#htmlTag").css("overflow-y", "hidden")
});

$('.menu-icon-black').on('click', function () {
    $(".overlayWeb").fadeOut("slow");
    $("#htmlTag").css("overflow-y", "overlay")
});
/* Menu - Web - Animations
https://stackoverflow.com/questions/63483562/play-video-on-hover-play-it-on-reverse-on-mouse-off
*/
$('.linkAnim').on('mouseover focus', function () {
    this.play();
    this.playbackRate = 3;
    this.parentElement.style.opacity = '1';
})

$('.linkAnim').on('mouseout blur', function () {
    var currentVideo = this;
    var videoParentId = $(this).parent().attr('id');
    var intervalRewind;
    intervalRewind = setInterval(function () {
        currentVideo.currentTime -= 0.1;
        if (currentVideo.currentTime == 0) {
            clearInterval(intervalRewind);
        }
    }, 15);
    currentVideo.pause();
    setTimeout(function () {
        $('#' + videoParentId).css('opacity', '0');
    }, 550);
})

/* WEB MENU END */

function openFullScreenMenu() {
    document.getElementById("myNav").style.height = "100%";
    document.getElementById("htmlTag").style.overflow = "hidden";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    document.getElementById("htmlTag").style.overflow = "auto";
}

function openFilterWeb() {
    document.getElementById("FilterWeb").style.height = "5%";
}

function closeFilterWeb() {
    document.getElementById("FilterWeb").style.height = "0%";
}

// Open and close filter menu

$("#filterBtn").click(function () {
    $("#filterMenu").fadeToggle();
    $("#filterChevron").toggleClass('fa fa-chevron-up');
    $("#filterChevron").toggleClass('fa fa-chevron-down');
    $("#filterBtn").toggleClass('filter-web-btn');
    $("#filterBtn").toggleClass('filter-web-btn-grayed');
});

// Open and close filter menu - mobile

$(".baleBTN").on("click", function (e) {
    if ($(".add-product").hasClass("open")) {
        $(".add-product").removeClass("open");
        $("#htmlTag").css("overflow", "auto");
    } else {
        $(".add-product").addClass("open");
        $("#htmlTag").css("overflow", "hidden");
    }
});

/* window.onload = openFilterMobile();

function openFilterMobile() {
    if (document.getElementById("overlayFilter").style.height == "0%") {
        document.getElementById("overlayFilter").style.height = "100%"
        document.getElementById('htmlTag').style.overflow = "hidden";
    } else {
        document.getElementById("overlayFilter").style.height = "0%"
        document.getElementById('htmlTag').style.overflow = "auto";
    }
} */