/* WEB MENU */

$('.menu-icon').on('click', function () {
        $(".overlayWeb").fadeIn("slow");
        $("#htmlTag").css("overflow-y", "hidden")
    }

);

$('.menu-icon-black').on('click', function () {
        $(".overlayWeb").fadeOut("slow");
        $("#htmlTag").css("overflow-y", "overlay")
    }

);
/* Menu - Web - Animations
https://stackoverflow.com/questions/63483562/play-video-on-hover-play-it-on-reverse-on-mouse-off
*/
$('.linkAnim').on('mouseover focus', function () {
    this.play();
    this.playbackRate = 3;
    this.parentElement.style.opacity = '1';
})

$('.linkAnim').on('mouseout blur', function () {
    var currentVideo = this;
    var videoParentId = $(this).parent().attr('id');
    var intervalRewind;
    intervalRewind = setInterval(function () {
        currentVideo.currentTime -= 0.1;
        if (currentVideo.currentTime == 0) {
            clearInterval(intervalRewind);
        }
    }, 15);
    currentVideo.pause();
    setTimeout(function () {
        $('#' + videoParentId).css('opacity', '0');
    }, 550);
})

/* WEB MENU END */

// header height 100vh

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);


/* Menu - Web - Animations
https://stackoverflow.com/questions/63483562/play-video-on-hover-play-it-on-reverse-on-mouse-off
*/


function openFullScreenMenu() {
    document.getElementById("myNav").style.height = "100%";
    document.getElementById('htmlTag').style.overflow = "hidden";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    document.getElementById('htmlTag').style.overflow = "auto";
}