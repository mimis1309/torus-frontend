//PRELOADER

//Something you want delayed.
var width = 100,
    perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
    EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
    time = parseInt((EstimatedTime / 1000) % 70) * 100;

// Load bar Animation
$(".loadbar").animate({
        width: width + "%",
    },
    time
);

// Fading Out Load bar on Finished
setTimeout(function () {
    $(".preloader-wrap").fadeOut(500);
}, time);


// header height 100vh

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
window.onresize = document.getElementById('headerSection').style.height = 'calc(var(--vh, 1vh) * 100)';


/* HASHTAG LINK */

var pageURL = document.URL;

//console.log(document.getElementById('mapContainer').offsetTop);

var sectionID = pageURL.substring(pageURL.lastIndexOf('#') + 1);

var n = pageURL.includes('#');

//console.log('demo - ' + n);
//console.log('sectionID - ' + sectionID);

/* NEWS LINK KLIK ZA TOCNO POZICIONIRANJE */

function showNews() {
    var newsSec = document.getElementById('news');
    var newsSecScroll = newsSec.offsetTop + 400;

    window.scrollTo(0, newsSecScroll);
}

/* NEWS LINK KLIK ZA TOCNO POZICIONIRANJE END*/

var bodyDiv = document.getElementById('bodyDiv');

/* if (n == true) {
  //console.log('sss');
  //console.log(sectionID);
    bodyDiv.classList.remove('fixedBody');
    document.getElementById(sectionID + 'ID').click();
} */



/* Menu - Web - Animations
https://stackoverflow.com/questions/63483562/play-video-on-hover-play-it-on-reverse-on-mouse-off
*/
/* var intervalRewind;
var parentE = $('.menu-anim');
 */
/* var intervalRewind;
var parent = $('.menu-anim');

$('.linkAnim')
    .on('mouseenter focus', function (event) {
    this.play();
    this.parentElement.style.opacity = '1';
    })
    .on('mouseout blus', function (event) {
    var currentVideo = this;
    var videoParentId = $(this).parent().attr('id');
    intervalRewind = setInterval(function () {
    currentVideo.pause();
    currentVideo.currentTime -= 0.1;
    if (currentVideo.currentTime == 0) {
        clearInterval(intervalRewind);
    }
    }, 30);

    setTimeout(function () {
    $('#' + videoParentId).css('opacity', '0');
    }, 650);
  }); */

/* RABOTEN KOD */

$('.linkAnim').on('mouseover focus', function () {
    this.play();
    this.playbackRate = 3;
    this.parentElement.style.opacity = '1';
})

$('.linkAnim').on('mouseout blur', function () {
    var currentVideo = this;
    var videoParentId = $(this).parent().attr('id');
    var intervalRewind;
    intervalRewind = setInterval(function () {
        currentVideo.currentTime -= 0.1;
        if (currentVideo.currentTime == 0) {
            clearInterval(intervalRewind);
        }
    }, 15);
    currentVideo.pause();
    setTimeout(function () {
        $('#' + videoParentId).css('opacity', '0');
    }, 550);
})

/* Menu - mobile*/

function openFullScreenMenu() {
    document.getElementById('myNav').style.height = '100%';
    document.getElementById('htmlTag').style.overflow = 'hidden';
}

function closeNav() {
    document.getElementById('myNav').style.height = '0%';
    document.getElementById('htmlTag').style.overflow = 'auto';
}

/* Carousel Swipe + hashtag links */
function projectConst() {
    closeNav();
    if (document.getElementById("archSlide").classList.contains('active')) {
        document.getElementById('carouselLeft').click();
    } else if (document.getElementById("restSlide").classList.contains('active')) {
        document.getElementById('carouselRight').click();
    }
}

function projectArch() {
    closeNav();
    if (document.getElementById("constSlide").classList.contains('active')) {
        document.getElementById('carouselRight').click();
    } else if (document.getElementById("restSlide").classList.contains('active')) {
        document.getElementById('carouselLeft').click();
    }
}

function projectRest() {
    closeNav();
    if (document.getElementById("constSlide").classList.contains('active')) {
        document.getElementById('carouselLeft').click();
    } else if (document.getElementById("archSlide").classList.contains('active')) {
        document.getElementById('carouselRight').click();
    }
}


/* Carousel Swipe + hashtag links END*/

/* Menu - mobile - SWIPE EFFECT*/
$('.carousel').carousel({
    touch: true // default
})

/* Menu - mobile - SWIPE EFFECT END*/

/*
Scroll to Menu with one wheel turn
http://jsfiddle.net/honk1/gWnNv/7/
https://deepmikoto.com/coding/1--javascript-detect-mouse-wheel-direction
*/

/* DETECT SCROLL DIRECTION + OPEN MENU WEB ON SCROLL DOWN */


if ('ontouchstart' in document.documentElement) {
    bodyDiv.classList.remove('fixedBody');
    document.getElementById('menuWeb').style.display = 'none';

    function openMenu() {
        document.getElementById('menuWebTouch').scrollIntoView();
    }

    function closeMenu() {
        document.getElementById('headerSection').scrollIntoView();
    }
} else {
    //netac
    document.getElementById('menuWebTouch').style.display = 'none';

    function openMenu() {
        document.getElementById('headerSection').style.transform = 'translateY(-100%)';
        bodyDiv.classList.remove('fixedBody');
        setTimeout(function () {
            document.getElementById('menu').style.opacity = '1';
            document.getElementById('closeMenu').style.opacity = '1';
        }, 500);
    }

    function closeMenu() {
        document.getElementById('headerSection').style.transform = 'translateY(0%)';
        bodyDiv.classList.add('fixedBody');
        setTimeout(function () {
            document.getElementById('menu').style.opacity = '0';
            document.getElementById('closeMenu').style.opacity = '0';
        }, 500);
    }
    window.addEventListener('scroll', menuUpDown());

    function menuUpDown() {
        function detectMouseWheelDirection(e) {
            var delta = null,
                direction = false;
            if (!e) {
                // if the event is not provided, we get it from the window object
                e = window.event;
            }
            if (e.wheelDelta) {
                // will work in most cases
                delta = e.wheelDelta / 60;
            } else if (e.detail) {
                // fallback for Firefox
                delta = -e.detail / 2;
            }
            if (delta !== null) {
                direction = delta > 0 ? 'up' : 'down';
            }
            return direction;
        }

        function handleMouseWheelDirection(direction) {
            var headerSection = document.getElementById('headerSection');
            if (direction == 'down') {
                headerSection.style.transform = 'translateY(-100%)';
                headerSection.style.transform = 'transition: 0.5s ease-in-out;';
                setTimeout(function () {
                    document.getElementById('menu').style.opacity = '1';
                    document.getElementById('closeMenu').style.opacity = '1';
                }, 500);

                setTimeout(function () {
                    bodyDiv.classList.remove('fixedBody');
                }, 500);
                // do something, like show the next page
            } else if (direction == 'up') {
                var menuWebTop = document.getElementById('menuWeb').getBoundingClientRect().top;
                if (menuWebTop == 0) {
                    headerSection.style.transform = 'translateY(0%)';
                    headerSection.style.transform = 'transition: 0.5s ease-in-out;';
                    setTimeout(function () {
                        document.getElementById('menu').style.opacity = '0';
                        document.getElementById('closeMenu').style.opacity = '0';
                    }, 500);
                    setTimeout(function () {
                        bodyDiv.classList.add('fixedBody');
                    }, 500);
                }
            } else {
                // this means the direction of the mouse wheel could not be determined
            }
        }
        document.onmousewheel = function (e) {
            handleMouseWheelDirection(detectMouseWheelDirection(e));
        };
        if (window.addEventListener) {
            document.addEventListener('DOMMouseScroll', function (e) {
                handleMouseWheelDirection(detectMouseWheelDirection(e));
            });
        }
    }
}

// Block 3 SLider

/* https://animate.style/#documentation
https://www.tylerfinck.com/
    DETECT IF TOUCH SCREEN
https://codepen.io/tteske/pen/KKwxOxp
*/

var projectsSlider = document.getElementById('verticalSlider');
var projectsSliderMobile = document.getElementById('verticalSliderMobile');


if ('ontouchstart' in document.documentElement) {
    projectsSlider.style.display = 'none';
    projectsSliderMobile.style.display = 'block';
} else {
    projectsSlider.style.display = 'flex';
    projectsSliderMobile.style.display = 'none';
}


function scrollAppearTest() {

    var introArchImg = document.getElementById('lazyphoto2');
    var introConstImg = document.getElementById('lazyphoto1');
    var introRestImg = document.getElementById('lazyphoto3');

    var introArchPositionTop = introArchImg.getBoundingClientRect().top;
    var introConstPositionTop = introConstImg.getBoundingClientRect().top;
    var introRestPositionTop = introRestImg.getBoundingClientRect().top;

    var archText = document.getElementById('architectureText');
    var consText = document.getElementById('constructionText');
    var restText = document.getElementById('realestateText');

    var consTextShownContent = consText.clientHeight;
    var archTextShownContent = archText.clientHeight;
    var restTextShownContent = restText.clientHeight;

    var current_li = 0;

    $("#rightClick").click(function () {
        if (consTextShownContent != 0) {
            current_li = 1;
        }
        if (archTextShownContent != 0) {
            current_li = 2;
        }
        if (restTextShownContent != 0) {
            current_li = 3;
        }
        if (current_li < 3) {
            current_li = current_li + 1;
            document.getElementById('lazyphoto' + current_li).scrollIntoView();
        } else if (current_li = 3) {
            /*   console.log("odi na prv" + 'lazyphoto' + 1); */
            window.scrollTo(0, window.innerHeight);
        }
    });

    $("#leftClick").click(function () {

        if (consTextShownContent != 0) {
            current_li = 1;
        }
        if (archTextShownContent != 0) {
            current_li = 2;
        }
        if (restTextShownContent != 0) {
            current_li = 3;
        }
        if (current_li > 1) {
            current_li = current_li - 1;
            window.scrollTo(0, document.getElementById('lazyphoto' + current_li).offsetTop);
        } else {
            document.getElementById('lazyphoto3').scrollIntoView();
        }
    });


    /*projects fixed buttons on the right side*/

    // var projectTopPosition = document.getElementById('verticalSlider').offsetTop;
    // var windowHight = $("#headerSection").height();
    // /*     projectTopPosition = projectTopPosition - windowHight - 50; */
    // var verticalSliderHeight = $("#verticalSlider").height();

    // var projectBottomPosition = projectTopPosition + verticalSliderHeight - windowHight;

    // $(function () {
    //     $(document).on("scroll", function () {
    //         var currentScrollTop = $(document).scrollTop();
    //         var currentScrollTop1 = window.innerHeight / 2;
    //         console.log("windows top scroll  " + currentScrollTop);
    //         console.log("projectTopPosition", projectTopPosition)

    //         if (projectTopPosition < currentScrollTop && projectBottomPosition > currentScrollTop) {
    //             $(".projects-btns").css({
    //                 "display": "block",
    //                 "position": "fixed",
    //                 "bottom": "150px;"
    //             });
    //         } else {
    //             $(".projects-btns").css({
    //                 "position": "absolute",
    //                 "bottom": "150px;"
    //             });
    //         }
    //     });
    // });

    /*projects fixed buttons on the right side end*/

    var screenPosition1 = window.innerHeight / 2;

    if (introConstPositionTop < screenPosition1) {
        consText.classList.add('construction-text1');
        $(".projects-btns").css({
            "display": "block",
            "position": "fixed",
            "bottom": "150px;"
        });
    } else {
        consText.classList.remove('construction-text1');
        $(".projects-btns").css({
            "position": "absolute",
            "bottom": "150px;"
        });
    }

    if (introArchPositionTop < screenPosition1) {
        archText.classList.add('architecture-text1');
        consText.classList.remove('construction-text1');
    } else {
        archText.classList.remove('architecture-text1');
    }

    if (introRestPositionTop < screenPosition1) {
        archText.classList.remove('architecture-text1');
        restText.classList.add('realestate-text1');
    } else {
        restText.classList.remove('realestate-text1');
    }

    var archText = document.getElementById('architectureText');
    var consText = document.getElementById('constructionText');
    var restText = document.getElementById('realestateText');

    var introArchImg = document.getElementById('lazyphoto2');
    var introConstImg = document.getElementById('lazyphoto1');
    var introRestImg = document.getElementById('lazyphoto3');

    var introArchPositionTop = introArchImg.getBoundingClientRect().top;
    var introConstPositionTop = introConstImg.getBoundingClientRect().top;
    var introRestPositionTop = introRestImg.getBoundingClientRect().top;

    var screenPosition1 = window.innerHeight / 2;

    if (introConstPositionTop < screenPosition1) {
        consText.classList.add('construction-text1');
    } else {
        consText.classList.remove('construction-text1');
    }

    if (introArchPositionTop < screenPosition1) {
        archText.classList.add('architecture-text1');
        consText.classList.remove('construction-text1');
    } else {
        archText.classList.remove('architecture-text1');
    }

    if (introRestPositionTop < screenPosition1) {
        archText.classList.remove('architecture-text1');
        restText.classList.add('realestate-text1');
        consText.classList.remove('construction-text1');
    } else {
        restText.classList.remove('realestate-text1');
    }
    if (introRestPositionTop < 0) {
        $(".projects-btns").css({
            "position": "absolute",

        });
    }
}

window.addEventListener('scroll', scrollAppearTest);


/*Block 6 NEWS */

//  DETECT IF TOUCH SCREEN
//  https://codepen.io/tteske/pen/KKwxOxp

var newsSlider = document.getElementById('news');
var newsSliderMobile = document.getElementById('newsSlider');
var careerSection = document.getElementById('careerContainer');

if ('ontouchstart' in document.documentElement) {
    newsSlider.style.display = 'none';
    newsSliderMobile.style.display = 'block';
    careerSection.style.opacity = '1';
} else {
    newsSlider.style.display = 'block';
    newsSliderMobile.style.display = 'none';
}

function newsEffects() {
    /*varijabli za promena na pozadina*/
    var newsSection = document.getElementById('news');
    var newsSectionTop = newsSection.getBoundingClientRect().top;
    var newsSectionBottom = newsSection.getBoundingClientRect().bottom;
    var newsContent = document.getElementById('news-content');
    var newsText = document.getElementById('newsText');
    var clientsSlider = document.getElementById('clientsSlider');
    var careerSection = document.getElementById('careerContainer');
    var backgroundDiv = document.getElementById('blackBg');
    //var screenPos1 = window.innerHeight / 2;
    var screenPos2 = window.innerHeight / 1.3;
    var screenPos3 = window.innerHeight / 7;

    //console.log("screenPos1 " + screenPos1);
    //console.log("screenPos2 " + screenPos2);
    //console.log("screenPos3 " + screenPos3);
    // console.log("newsSectionTop " + newsSectionTop);
    /* varijabli za promena na pozadina*/

    /*presmetka na width od boxes*/
    var box = document.getElementsByClassName('news-projects-selection');
    var boxWidth = box[0].clientWidth;
    var numberOfBoxes = box.length;
    var boxesWidth = (boxWidth + 40) * numberOfBoxes;
    /*presmetka na width od boxes*/

    /*zadavanje sirina i visina na news-slider i news*/
    document.getElementById('news-content').style.width = boxesWidth + 'px';
    document.getElementById('news').style.height = boxesWidth + 'px';
    /*zadavanje sirina na news-slider*/

    /*detektiranje scrollTop*/
    var currentScroll = window.scrollY;
    var topNews = document.getElementById('news').offsetTop;
    currentScroll = currentScroll - topNews - 700;
    /*detektiranje scrollTop*/

    var scroll30 = window.innerHeight * 0.50;
    //console.log("newsEffects -> scroll30", scroll30)

    if (newsSectionTop < 0) {
        // console.log("newsEffects -> newsSectionTop", newsSectionTop);
        // od kade da pocuva promenata
        if (newsSectionBottom > screenPos2) {
            // se duri ne go dostigneme bottom drezi go vnatre
            newsContent.classList.add('newsContent-fixed');
            newsText.classList.add('news-text-fixed');
            newsSection.classList.add('front-black');
            backgroundDiv.classList.add('black-bg');
            clientsSlider.classList.add('slider-wanish');
            $('#news-content').css('left', -currentScroll);
            // console.log("----- currentScroll - " + currentScroll);
            if (currentScroll >= -scroll30) {
                // console.log(">=300 - " + currentScroll);
                // console.log("fiksirame");
                $('#news-content').css('top', '25%');
                $('#newsText').css('top', '25%');
                newsText.classList.add('news-text-fixed');
                var scrollPer = window.innerWidth * 0.21;
                var newPosition = currentScroll / 7 - scrollPer;
                $('#newsText').css('left', -newPosition);
            } else {
                $('#news-content').css('top', -currentScroll / 2); // kolku visoko da pocnuva news Content
                $('#newsText').css('left', 0);
                $('#newsText').css('top', 0);
                // console.log("odime nagore");
                document.getElementById('newsText').classList.remove('news-text-fixed');
                // console.log("<300 - " + currentScroll);
                // console.log("odime nagore");
            }
        } else {
            //$("#newsText").css("position", "absolute !important");
            newsContent.classList.remove('newsContent-fixed');
            newsText.classList.remove('news-text-fixed');
            backgroundDiv.classList.remove('black-bg');
            careerSection.classList.add('career-show');
            document.getElementById('newsText').classList.remove('news-text-fixed');
        }
    } else if (newsSectionTop > -200) {
        newsContent.classList.remove('newsContent-fixed');
        newsText.classList.remove('news-text-fixed');
        backgroundDiv.classList.remove('black-bg');
        clientsSlider.classList.remove('slider-wanish');
        newsSection.classList.remove('front-black');
        careerSection.classList.remove('career-show');
    }
}

window.addEventListener('scroll', newsEffects);

/*Block 6 NEWS TEST*/

// function newsEffects() {
//     /*varijabli za promena na pozadina*/
//     var newsSection = document.getElementById('news');
//     var newsSectionTop = newsSection.getBoundingClientRect().top;
//     var newsSectionBottom = newsSection.getBoundingClientRect().bottom;
//     var newsContent = document.getElementById('news-content');
//     var newsText = document.getElementById('newsText');
//     var clientsSlider = document.getElementById("clientsSlider");
//     var careerSection = document.getElementById('careerContainer');
//     var backgroundDiv = document.getElementById('blackBg');
//     //var screenPos1 = window.innerHeight / 2;
//     var screenPos2 = window.innerHeight / 1.3;
//     var screenPos3 = window.innerHeight / 7;

//     //console.log("screenPos1 " + screenPos1);
//     console.log("screenPos2 " + screenPos2);
//     console.log("screenPos3 " + screenPos3);
//     console.log("newsSectionTop " + newsSectionTop);
//     /* varijabli za promena na pozadina*/

//     /*presmetka na width od boxes*/
//     var box = document.getElementsByClassName("news-projects-selection");
//     var boxWidth = box[0].clientWidth;
//     var numberOfBoxes = box.length;
//     var boxesWidth = (boxWidth + 40) * numberOfBoxes + 1400;
//     /*presmetka na width od boxes*/

//     /*zadavanje sirina i visina na news-slider i news*/
//     document.getElementById('news-content').style.width = boxesWidth + "px";
//     document.getElementById('news').style.height = boxesWidth + "px";
//     /*zadavanje sirina na news-slider*/

//     /*detektiranje scrollTop*/
//     var currentScroll = window.scrollY;
//     var topNews = document.getElementById('news').offsetTop;
//     currentScroll = currentScroll - topNews - 800;
//     var currentScrollText = (currentScroll + 800) / 50;
//     /*detektiranje scrollTop*/

//     if (newsSectionTop < 0 && newsSectionBottom > screenPos2) {
//         newsSection.classList.add("front-black");
//         backgroundDiv.classList.add('black-bg');
//         clientsSlider.classList.add('slider-wanish');
//         $("#news-content").css("left", -currentScroll);
//         $("#newsText").css("left", -currentScrollText);

//         console.log("------------------------ " + currentScroll);
//         if (currentScroll < -300) {
//             $("#news-content").css("top", -currentScroll);
//             $("#newsText").css("top", -currentScrollText - 300);
//             $("#newsText").css("position", "absolute");
//             $("#newsText").css("top", "revert");
//             $("#newsText").css("left", "10px");
//             console.log("odime nagore");
//         } else if (currentScroll >= -300) {

//             console.log("fiksirame");
//             $("#news-content").css("top", "300px");
//             $("#news-content").css("position", "fixed");
//             //$("#newsText").css("left", "500px");
//             $("#newsText").css("position", "fixed");
//             $("#newsText").css("top", "30%");
//             $("#newsText").css("left", "46%");
//             $("#newsText").css("left", -currentScrollText);
//         } else {
//             $("#news-content").css("position", "absolute");

//         }

//     } else {
//         newsSection.classList.remove("front-black");
//         newsContent.classList.remove('newsContent-fixed');
//         //newsText.classList.remove('news-text-fixed');
//         clientsSlider.classList.remove('slider-wanish');
//         backgroundDiv.classList.remove('black-bg');
//         careerSection.classList.remove('career-show');
//     }

//     if (newsSectionTop < -200) {
//         newsContent.classList.add('newsContent-fixed');
//         newsText.classList.add('news-text-fixed');
//     }

//     if (newsSectionBottom < screenPos2) {
//         newsSection.classList.remove("front-black");
//         backgroundDiv.classList.remove('black-bg');
//         careerSection.classList.add('career-show');
//     } else {
//         careerSection.classList.remove('career-show');
//     }
// }

// window.addEventListener('scroll', newsEffects);

//map
//kreira mapa
map = L.map("mapid", {
    scrollWheelZoom: false
}).setView([0, 0], 1)
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWltaXMxMzA5IiwiYSI6ImNrZzloeGUzbjAwNWMydHN3dWd1MnU3aWYifQ.K4HdgOHF5bl0KaMWLfYxHw', {
    id: 'mapbox/light-v9',

    tileSize: 512,
    zoomOffset: -1
}).addTo(map);
var leafletIcon = L.icon({
    iconUrl: 'img/favicon/favicon.ico',
    iconSize: [30, 30],
})
var allMarkers = L.featureGroup();
var projects = [{
        "LocationID": 0,
        "NameMK": "Skopje City Mall",
        "ShortDMK": "4ff84549-6db1-4e32-8d80-9c8527d2759a",
        "PageUrl": false,
        "Image": "img/career-media/pic1.jpg",
        "Latitude": 41.997345,
        "Longitude": 21.427996,
        "ProjectLink": "Project/2project.html"
    },
    {
        "LocationID": 1,
        "NameMK": "Skopje City Mall",
        "ShortDMK": "16486d94-5300-4da3-b02e-34703cbea4a3",
        "PageUrl": true,
        "Image": "img/career-media/pic1.jpg",
        "Latitude": 41.123096,
        "Longitude": 20.801647,
        "ProjectLink": "Project/2project.html"
    },
    {
        "LocationID": 2,
        "NameMK": "Skopje City Mall",
        "ShortDMK": "fffe4542-66ab-433d-ade9-41ca17644ce9",
        "PageUrl": false,
        "Image": "img/career-media/pic1.jpg",
        "Latitude": 44.786568,
        "Longitude": 20.448921,
        "ProjectLink": "Project/2project.html"
    },
    {
        "LocationID": 3,
        "NameMK": "Skopje City Mall",
        "ShortDMK": "c5bcd147-7eef-45a2-89e5-2dabf2b96375",
        "PageUrl": true,
        "Image": "img/career-media/pic1.jpg",
        "Latitude": 42.697708,
        "Longitude": 23.321867,
        "ProjectLink": "Project/2project.html"
    },
    {
        "LocationID": 4,
        "NameMK": "Skopje City Mall",
        "ShortDMK": "63e39582-d800-4c3c-b2fe-711785cbede8",
        "PageUrl": false,
        "Image": "img/career-media/pic1.jpg",
        "Latitude": 37.980739,
        "Longitude": 23.739223,
        "ProjectLink": "Project/2project.html"
    }
]


for (var i = 0; i < projects.length; i++) {
    var lat = projects[i].Latitude;
    var lng = projects[i].Longitude;
    // treba proveruvanje lokalizacija
    var name = projects[i].NameMK;
    var shortD = projects[i].ShortDMK;
    if (projects[i].Image != null) {
        var img = projects[i].Image;
    } else {
        var img = null;
    }
    var pageUrl = projects[i].PageUrl;


    var thisMarker = L.marker([lat, lng]);
    thisMarker.addTo(allMarkers);


    var marker = L.marker([lat, lng], {
        icon: leafletIcon,
        win_url: projects[i].ProjectLink
    }).addTo(map);

    marker.bindPopup('<div class="content">' +

        '<h1 id="firstHeading" class="firstHeading">' + name + '</h1>' +
        '<div class="siteNotice">' +
        '</div>' +
        /*  '<div class="bodyContent">' +
        '<p>' + shortD + '</p>' +
         '<a href="/igroteka/lokacija/' + pageUrl + '"' + '>Види Профил</a>' + */
        '</div>' +
        '</div>', {
            Width: "250px"
        });
    marker.on('mouseover', function (e) {
        this.openPopup();
    });
    marker.on('mouseout', function (e) {
        this.closePopup();
    });
    marker.on('click', onClick);

}

function onClick() {
    console.log(this.options.win_url);
    window.open(this.options.win_url);
}

map.fitBounds(allMarkers.getBounds().pad(0.3));