/* WEB MENU */

$('.menu-icon').on('click', function () {
        $(".overlayWeb").fadeIn("slow");
        $("#htmlTag").css("overflow-y", "hidden")
    }

);

$('.menu-icon-black').on('click', function () {
        $(".overlayWeb").fadeOut("slow");
        $("#htmlTag").css("overflow-y", "overlay")
    }

);
/* Menu - Web - Animations
https://stackoverflow.com/questions/63483562/play-video-on-hover-play-it-on-reverse-on-mouse-off
*/
$('.linkAnim').on('mouseover focus', function () {
    this.play();
    this.playbackRate = 3;
    this.parentElement.style.opacity = '1';
})

$('.linkAnim').on('mouseout blur', function () {
    var currentVideo = this;
    var videoParentId = $(this).parent().attr('id');
    var intervalRewind;
    intervalRewind = setInterval(function () {
        currentVideo.currentTime -= 0.1;
        if (currentVideo.currentTime == 0) {
            clearInterval(intervalRewind);
        }
    }, 15);
    currentVideo.pause();
    setTimeout(function () {
        $('#' + videoParentId).css('opacity', '0');
    }, 550);
})

/* WEB MENU END */

// header height 100vh

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

function openFullScreenMenu() {
    document.getElementById("myNav").style.height = "100%";
    document.getElementById('htmlTag').style.overflow = "hidden";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    document.getElementById('htmlTag').style.overflow = "auto";
}


// Counters
/* https://www.youtube.com/watch?v=a6XIMIKmj9k&ab_channel=TraversyMedia
https://codepen.io/bradtraversy/pen/poJwqOK
For when visible -- -- https://usefulangle.com/post/113/javascript-detecting-element-visible-during-scroll */

window.addEventListener('scroll', function () {
    var factsContainer = document.getElementById('factsYear');
    var factsContainer = factsContainer.getBoundingClientRect();
    var yearAppear = document.getElementById('factsFounded');
    if (factsContainer.top >= 0 && factsContainer.bottom <= window.innerHeight) {
        const counters = document.querySelectorAll('.counter-year');
        const speed = 3000;
        counters.forEach(counter => {
            const updateCount = () => {
                const target = +counter.getAttribute('data-target');
                const count = +counter.innerText;
                const inc = target / speed;

                if (count < target) {
                    counter.innerText = Math.ceil(count + inc);
                    setTimeout(updateCount, 1);
                } else {
                    counter.innerText = target;
                }
            };
            updateCount();
        });
    };
    if (factsContainer.bottom <= window.innerHeight) {
        yearAppear.classList.add('facts-founded-appear');
    };
});

/* Counter for Area with thousand separator */

window.addEventListener('scroll', function () {
    var factsContainer = document.getElementById('factsArea');
    var factsContainer = factsContainer.getBoundingClientRect();
    var projectsAppear = document.getElementById('factsAreaBuilt');

    if (factsContainer.top >= 0 && factsContainer.bottom <= window.innerHeight) {
        const genNumber = () => {
            document.querySelector(".elvis").style.setProperty("--percent", 3);
        };

        setInterval(genNumber, 2000);
        setTimeout(genNumber);
    };
    if (factsContainer.bottom <= window.innerHeight) {
        projectsAppear.classList.add('facts-founded-appear');
    };
});

window.addEventListener('scroll', function () {
    var factsContainer = document.getElementById('factsProjects');
    var factsContainer = factsContainer.getBoundingClientRect();
    var projectsAppear = document.getElementById('factsProjectsDone');

    if (factsContainer.top >= 0 && factsContainer.bottom <= window.innerHeight) {
        const counters = document.querySelectorAll('.counter-projects');
        const speed = 1000;

        counters.forEach(counter => {
            const updateCount = () => {
                const target = +counter.getAttribute('data-target');
                const count = +counter.innerText;
                const inc = target / speed;

                if (count < target) {
                    counter.innerText = Math.ceil(count + inc);
                    setTimeout(updateCount, 1);
                } else {
                    counter.innerText = target;
                }
            };
            updateCount();
        });
    };
    if (factsContainer.bottom <= window.innerHeight) {
        projectsAppear.classList.add('facts-founded-appear');
    };
});

/* Gallery pagination

http://jsfiddle.net/Lf9krnsm/
https://stackoverflow.com/questions/27304764/bootstrap-3-carousel-with-page-numbers */

var total = $('.carousel-item').length;
var currentIndex = $('div.active').index() + 1;
$('#slidetext1').html(currentIndex);
$('#slidetext2').html(total);

// This triggers after each slide change
$('.carousel').on('slid.bs.carousel', function () {
    currentIndex = $('div.active').index() + 1;

    // Now display this wherever you want
    var textCurrentIndex = currentIndex;
    var textTotal = total;

    $('#slidetext1').html(textCurrentIndex);
    $('#slidetext2').html(textTotal);

});

/* Menu - mobile - SWIPE EFFECT*/
$('.carousel').carousel({
    touch: true // default
})


/* LICENSES Galery
http://www.landmarkmlp.com/js-plugin/owl.carousel/demos/full.html
https://www.youtube.com/watch?v=1LZngKzUOoY&ab_channel=RootSploit
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_js_lightbox */

$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 50,
    nav: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        },
        1800: {
            items: 6
        }
    }
})

// LightBox Popup

function openModal() {
    document.getElementById("myModal").style.display = "block";
}

function closeModal() {
    document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace("active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    //dots[slideIndex - 1].className += " active";
    //captionText.innerHTML = dots[slideIndex - 1].alt;
}